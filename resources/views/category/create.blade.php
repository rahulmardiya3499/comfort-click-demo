@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Create Event
                <a href="{{ route('categories.index') }}" class="btn btn-primary float-right">Event List</a>
            </div>
            <div class="card-body">
                <form action="{{ route('categories.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Category Name</label>
                            <input type="text" name="name" class="form-control">
                            @error('name')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }} </strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label>Select Parent Category </label>
                            <select id="parent_id" name="parent_id" class="form-control">
                                <option value="">select</option>
                                @foreach ($categories as $category)
                                    @include('category.subcategoryoption', [
                                        'category' => $category,
                                        'disableParents' => true,
                                        'editingCategory' => null,
                                    ])
                                @endforeach
                            </select>
                            @error('parent_id')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>~
                    <div class="row ">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success"> Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
