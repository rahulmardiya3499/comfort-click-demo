@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <span class="float-left">Categories list</span> 
            <a href="{{route('categories.create')}}" class="btn btn-primary float-right">Add</a>
        </div>
        <div class="card-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No. </th>
                        <th>Name</th>
                        <th>Parent Category</th>
                        <th>Status</th>
                        <th>Created Time</th>
                        <th>Last Updated Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=1
                    @endphp
                    @forelse($categories as $category)
                        <tr>
                            <th>{{$i++}}</th>
                            <td>{{$category->name}}
                            </td>
                            <td>{{$category->parent->name??'N/A'}}</td>
                            <td> 
                                @if($category->status)
                                    <span class="badge badge-success"> Active 
                                @else 
                                    <span class="badge badge-danger"> Inactive 
                                @endif
                            </td>
                            <td> {{date('d-m-Y',strtotime($category->created_at))}} </td>
                            <td> {{date('d-m-Y',strtotime($category->updated_at))}} </td>
                            <th>
                                <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-primary float-left">Edit</a>
                                
                                <form id="logout-form" action="{{ route('categories.destroy',$category->id) }}" method="POST">
                                    @csrf 
                                    @method('DELETE')
                                    <button  type="submit" class="btn btn-danger float-right" onclick="return confirm('Are you sure delete this recored? If you delete this category you will lost all sub category.');"> Delete </button>
                                </form>

                            </th>
                        </tr>
                    @empty
                    <tr>
                        <td colspan="5">
                            <span class="text-center"> Data Not Available!!</span>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
       
    </div>
</div>
@endsection
