@php
    $isDisabled = null;
    if($editingCategory!= null)
        $isDisabled = $disableParents && $category->id == $editingCategory->id;
@endphp

<option value="{{ $category->id }}" {{ $isDisabled ? 'disabled' : '' }} @if(!empty($editingCategory) && $editingCategory->parent_id == $category->id) selected @endif>
    {{ $category->getFullPath() }}
</option>

@if ($category->children)
    @foreach ($category->children as $child)
        @include('category.subcategoryoption', ['category' => $child, 'disableParents' => $disableParents, 'editingCategory' => $editingCategory])
    @endforeach
@endif