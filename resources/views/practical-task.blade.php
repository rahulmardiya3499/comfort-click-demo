@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">
            Practical Task 1 
        </div>
        <div class="card-body">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th> Input </th>
                        <th> output </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @php
                                $inputArray = [
                                    "English" => "Subject",
                                    "India" => "Country",
                                    "Maths" => "Subject",
                                    "USA" => "Country",
                                    "Canada" => "Country",
                                    "Pen" => "Stationary"
                                ];
                                print_r($inputArray);
                            @endphp
                        </td>
                        <td>
                            @php
                                $outputArray = [];
                                foreach ($inputArray as $key => $value) { 
                                    if (!isset($outputArray[$value])) {
                                        $outputArray[$value] = [];
                                    }
                                    if(array_key_exists('Stationary',$outputArray))
                                    {
                                        $outputArray[$value][] = "Subject";
                                    }else{
                                        $outputArray[$value][] = $key;
                                    }
                                    
                                }
                                // Output the result
                                echo "Output Array -\n";
                                print_r($outputArray);
                            @endphp
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @php
                                $inputString = "aabbbccaaaac";
                                echo $inputString;
                            @endphp
                        </td>
                        <td>
                            @php
                                function encodeString($inputString) {
                                    $outputString = '';
                                    $length = strlen($inputString);
                                    
                                    if ($length === 0) {
                                        return $outputString;
                                    }

                                    $currentChar = $inputString[0];
                                    $count = 1;
                                    
                                    for ($i = 1; $i < $length; $i++) {
                                        if ($inputString[$i] === $currentChar) {
                                            $count++;
                                        } else {
                                            $outputString .= ($count > 1 ? $count : '') . $currentChar;
                                            $currentChar = $inputString[$i];
                                            $count = 1;
                                        }
                                    }
                                    
                                    $outputString .= ($count > 1 ? $count : '') . $currentChar;
                                
                                    return $outputString;
                                }
                                $encodedOutput = encodeString($inputString);
                                echo "Output: $encodedOutput";

                            @endphp
                        </td>
                    </tr>
                </tbody>
            </table>
          
        </div>
    </div>
</div>

@endsection