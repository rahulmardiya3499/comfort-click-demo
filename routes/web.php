<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware('auth')->group(function(){
    Route::resource('categories',CategoryController::class);
    Route::get('practical-task-1',[CategoryController::class,'practicalTask1'])->name('practical.task.1');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
