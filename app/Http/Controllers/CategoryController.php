<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    
    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('category.index', compact('categories'));
    }

    public function create()
    {
        $categories = Category::whereNull('parent_id')->with('children')->get();
        return view('category.create',compact('categories'));
    }

    public function store(StoreCategoryRequest $request)
    {
        $category = array(
            'name'=>$request->name,
            'description'=>$request->description,
            'parent_id'=>$request->parent_id,
        );
        $categories = $this->categoryRepository->create($category);
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = $this->categoryRepository->findById($id);
        $categories = Category::whereNull('parent_id')->with('children')->get();
        return view('category.edit', compact('category','categories'));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = array(
            'name'=>$request->name,
            'description'=>$request->description,
            'parent_id'=>$request->parent_id,
        );
        $categories = $this->categoryRepository->update($id,$category);
        return redirect()->route('categories.index');
    }

    public function destroy($id)
    { 
        $category = Category::find($id);
        if ($category) {
            $category->deleteWithSubcategories();
            return redirect()->route('categories.index');
        }
    }

    public function practicalTask1()
    {
        return view('practical-task');
    }

}
