<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{
    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    public function all()
    {
        return $this->model->with('children')->orderBy('created_at',)->get();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $attributes)
    {
        //dd($attributes);
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        $category = $this->findById($id);
        $category->update($attributes);
        return $category;
    }

    public function delete($id)
    {
        $category = $this->findById($id);
        $category->delete();
        // if ($category->parent_id) {
        //     foreach ($category->children()->with('category')->get() as $child) {
        //         foreach ($child->parent_id as $cat) {
        //             $cat->update(['category_id' => NULL]);
        //         }
        //     }
            
        //     $category->children()->delete();
        // }
        // foreach ($category->parent_id as $cat) {
        //     $cat->update(['category_id' => NULL]);
        // }
       
    }

    public function findSubCategoriesIDs($id)
    {
        $category = $this->findById($id);
        $ids = array();
        if(!empty($category->parent_id))
        {
            $categories =Category::where('parent_id',$id)->get();
            
            foreach($categories as $cat)
            {
                $cat1 = $this->findById($cat->id); 
            }
        }
    }

}
