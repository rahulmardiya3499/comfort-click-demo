<?php

namespace App\Repositories;

interface CategoryRepositoryInterface
{
    public function all();

    public function findById($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function delete($id);
}
